<?php

/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hotel-pasteur
 */

?>

<article id="sejour-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="visit-header" style="background-color: <?php the_field('color'); ?>">
        <?php if (has_post_thumbnail()) : ?>
            <img alt="" style="background-image: url(<?php the_post_thumbnail_url() ?>)" />
        <?php endif; ?>
    </div>
    <header class="entry-header">
        <?php the_title( '<h5 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' ); ?>
    </header><!-- .entry-header -->

    <footer class="entry-footer">
        <div>
            <?php
            $term = get_field('place');
            if ($term) : ?>
                <h6><?php echo esc_html($term->name); ?></h6>
            <?php endif; ?>
        </div>
        <div>
            <?php
            $terms = get_field('activity');
            if ($terms) : ?>
                    <?php foreach ($terms as $term) : ?>
                        <span class="s-chip"><?php echo esc_html($term->name); ?></span>
                    <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->