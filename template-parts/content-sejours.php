<?php

/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hotel-pasteur
 */

?>
<?php
// ACF Field and permalink variables declaration
$color = get_field('color');
$place = get_field('place');
$excerpt = get_field('excerpt');
$start_date = strtotime(get_field('start_date'));
$end_date = null;
$is_end_date = get_field_object('is_end_date')['value'];
if ($is_end_date == "true") {
    $end_date = strtotime(get_field('end_date'));
}
$activities = get_field('activity');
?>

<article id="sejour-<?php the_ID(); ?>" <?php post_class(); ?> <?php if (!has_post_thumbnail() && $color) : ?> style="border-top: 0.7rem solid <?php the_field('color');
                                                                                                                                            endif; ?>">
    <?php if (has_post_thumbnail()) : ?>
        <div class="visit-header" style="background-color: <?php if ($color) :  the_field('color');
                                                            endif; ?>">
            <img alt="" style="background-image: url(<?php the_post_thumbnail_url() ?>)" />
        </div>
    <?php endif; ?>
    <header class="entry-header">
        <?php the_title('<h5 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h5>'); ?>
    </header><!-- .entry-header -->
    <div class="sejour-meta">
        <h5>
            <?php if ($is_end_date == "true") : ?>
                Du <?php echo date_i18n("d F, Y", $start_date); ?> au <?php echo date_i18n("d F, Y", $end_date); ?>
            <?php else : ?>
                Depuis le <?php echo date_i18n("d F, Y", $start_date); ?>
            <?php endif ?>
        </h5>
        <div>
            <?php
            if ($place) : ?>
                <h6><?php echo esc_html($place->name); ?></h6>
            <?php endif; ?>
        </div>
        <div>
            <?php
            if ($activities) : ?>
                <?php foreach ($activities as $activity) : ?>
                    <span class="s-chip"><?php echo esc_html($activity->name); ?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div><!-- .sejour-meta -->
    <div class="entry-content">
        <?php
        if ($excerpt) {
            echo "<p>" . esc_html($excerpt) . "</p>";
        }
        ?>
    </div><!-- .entry-content -->

    <a class="sejour-link" href="<?php esc_url(the_permalink()) ?>" rel="bookmark">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
            <path fill="currentColor" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
        </svg>
    </a>
</article><!-- #post-<?php the_ID(); ?> -->