<?php

/**
 * Template part for displaying a sejour linked in le-registre web app
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hotel-pasteur
 */

?>
<?php

$currentPageUrl = 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
// Use parse_url() function to parse the URL 
// and return an associative array which
// contains its various components
$url_components = parse_url($currentPageUrl);

// Use parse_str() function to parse the
// string passed via URL
parse_str($url_components['query'], $params);

$index = $params['index'];

// ACF Field variables declaration
$color = get_field('color');
$places = get_field('place');
$start_date = strtotime(get_field('start_date'));
$end_date = null;
$is_end_date = false;
if (get_field_object('is_end_date')) {
    $is_end_date = get_field_object('is_end_date')['value'];
}


if ($is_end_date == "true") {
    $end_date = strtotime(get_field('end_date'));
}

$multiple_dates = get_field_object('multiple_dates');
$is_mult_date = get_field('is_mult_date');
if ($is_mult_date == 1) {
    $start_date = strtotime($multiple_dates["value"][$index]["start_date"]);
    $end_date = strtotime($multiple_dates["value"][$index]["end_date"]);
}

$activities = get_field('activity');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php if (has_post_thumbnail()) : ?>
            <img id="header-image" style="background-image: url(<?php the_post_thumbnail_url() ?>)" />
        <?php endif; ?>
        <div class="entry-subheader">
            <?php the_title('<h4 class="entry-title">', '</h4>'); ?>
        </div>
        <h5>
            <?php if ($is_end_date  == "true") : ?>
                <?php if ($start_date  == $end_date) : ?>
                    Le <?php echo date_i18n("d F, Y", $start_date); ?>
                <?php else : ?>
                    Du <?php echo date_i18n("d F, Y", $start_date); ?> au <?php echo date_i18n("d F, Y", $end_date); ?>
                <?php endif ?>
            <?php else : ?>
                Depuis le <?php echo date_i18n("d F, Y", $start_date); ?>
            <?php endif ?>
        </h5>

        <div>
            <?php
            if ($places && sizeof($places) > 2) : ?>
                <p> dans <b><?php echo esc_html($places[0]->name); ?></b> et <?php echo sizeof($places) - 1; ?> autres espaces</p>
            <?php
            elseif ($places && sizeof($places) == 2) : ?>
                <p> dans <b><?php echo esc_html($places[0]->name); ?></b> et un autre espace</p>
            <?php
            elseif ($places && sizeof($places) == 1) : ?>
                <p> dans <b><?php echo esc_html($places[0]->name); ?></p>
            <?php endif; ?>
        </div>
        <div>
            <?php
            if ($activities) : ?>
                <?php foreach ($activities as $activity) : ?>
                    <span class="s-chip"><?php echo esc_html($activity->name); ?></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
        the_content();
        ?>
    </div><!-- .entry-content -->

    <?php if (get_edit_post_link()) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __('Edit <span class="screen-reader-text">%s</span>', 'hotel-pasteur'),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    wp_kses_post(get_the_title())
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->