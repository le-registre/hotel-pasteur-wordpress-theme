<?php
/**
 *
 * The page that display the svelte web app le-registre
 * le-registre js and css style is loaded in function.php
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hotel-pasteur
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section id="le-registre"></section>
	</main><!-- #main -->

<?php
get_footer();
