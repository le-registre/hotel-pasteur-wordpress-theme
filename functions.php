<?php

/**
 * hotel-pasteur functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hotel-pasteur
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('hotel_pasteur_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hotel_pasteur_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hotel-pasteur, use a find and replace
		 * to change 'hotel-pasteur' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('hotel-pasteur', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'hotel-pasteur'),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);
		/*
		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'hotel_pasteur_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);
		*/
		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'hotel_pasteur_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hotel_pasteur_content_width()
{
	$GLOBALS['content_width'] = apply_filters('hotel_pasteur_content_width', 640);
}
add_action('after_setup_theme', 'hotel_pasteur_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hotel_pasteur_widgets_init()
{
	// We register two sidebars with id's footer and footer-2
	register_sidebars(
		2,
		array(
			'name' => 'Pied de Page %d',
			'before_sidebar' => '<div class="footer-widgets">',
			'after_sidebar' => '</div>',
			'before_widget' => '<div class="widget">',
			'after_widget' => '</div>',
			'id'            => 'footer',
			'description'   => esc_html__('Ajouter des widgets ici :', 'hotel-pasteur'),
		),
	);
}
add_action('widgets_init', 'hotel_pasteur_widgets_init');


/**
 * Enqueue scripts and styles.
 */
function hotel_pasteur_scripts()
{
	wp_enqueue_style('hotel-pasteur-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('hotel-pasteur-style', 'rtl', 'replace');
	wp_enqueue_script('hotel-pasteur-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

	// Include Le Registre Svelte App bundle.js and bundle.css
	if (is_page('le-registre')) {
		wp_enqueue_script('hotel-pasteur-le-registre', get_template_directory_uri() . '/le-registre/public/build/bundle.js', array(), _S_VERSION, true);
		wp_enqueue_style('hotel-pasteur-le-registre-style', get_template_directory_uri() . '/le-registre/public/build/bundle.css', array(), _S_VERSION);
		wp_enqueue_style('hotel-pasteur-le-registre-style', get_template_directory_uri() . '/le-registre/public/global.css', array(), _S_VERSION);
	}
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	// If we're not in admin panels, we remove jquery and dashicon for performance
	if (!is_admin())  wp_deregister_script('jquery');
}
add_action('wp_enqueue_scripts', 'hotel_pasteur_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}
/**
 * Limit file size upload
 */
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');
function max_file_size($file)
{
	$size = $file['size'];
	$size = $size / 1024;
	$type = $file['type'];
	$is_image = strpos($type, 'image') !== false;
	$is_audio = strpos($type, 'audio') !== false;
	$is_video = strpos($type, 'video') !== false;
	if ($is_image) {
		$limit = 2000;
		$limit_output = '2MB';
		if ($size > $limit) {
			$file['error'] = "Les fichiers image doivent peser moins de " . $limit_output;
		} //end if
	} else if ($is_audio) {
		$limit = 5000;
		$limit_output = '5MB';
		if ($size > $limit) {
			$file['error'] = 'Les fichiers audio doivent peser moins de ' . $limit_output;
		} //end if
	} else if ($is_video) {
		$limit = 50000;
		$limit_output = '50MB';
		if ($size > $limit) {
			$file['error'] = 'Les fichiers vidéo doivent peser moins de ' . $limit_output;
		} //end if
	}
	return $file;
} //end nelio_max_image_size()
add_filter('wp_handle_upload_prefilter', 'max_file_size');

// Remove dashicon from front
function wpdocs_dequeue_dashicon() {
	if (current_user_can( 'update_core' )) {
		return;
	}
	wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );