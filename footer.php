<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hotel-pasteur
 */

?>

<footer id="colophon" class="site-footer">

	<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e('Footer', 'twentynineteen'); ?>">
		<?php
		if (is_active_sidebar('footer')) {
		?>
			<?php dynamic_sidebar('footer'); ?>
		<?php
		}
		?>
		<?php
		if (is_active_sidebar('footer-2')) {
		?>
			<?php dynamic_sidebar('footer-2'); ?>
		<?php
		}
		?>
	</aside><!-- .widget-area -->
	<?php
	if (!is_front_page() && !is_home()) {
		the_custom_logo();
	}
	?>
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>