# Le Registre WordPress Theme

Contributors: Marin Esnault, automattic
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called hotel-pasteur.

## Description

Le thème WordPress du Registre de l\'Hôtel Pasteur !

Wordpress Theme of Le Registre for the Hôtel Pasteur.
Le Registre is an web application based on WordPress, Svelte and GraphQL.
It's a contributive map and agenda made for the guest to document their journey.

The wordpress theme is a wrapper for Le Registre svelte's web application.
It also need The Registre Wordpress plugin for custom post types and host administration panel.

## Installation

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
