<?php

/**
 * The template page that display several sejours (not used)
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hotel-pasteur
 */

get_header();
?>
<?php
$visits_args = array(
    'post_type'   => 'sejour',
    'meta_key' => 'start_date',
    'meta_type' => 'DATE',
    'posts_per_page' => 10,
    'orderby' => 'meta_value',
    'order' => 'DESC',
);
/*
    'post_type' => 'sejour',
    'posts_per_page' => -1,
    'author' => $author_id,
    'meta_key' => 'start_date',
    'meta_type' => 'DATE',
    'order' => 'DESC',
    'orderby' => 'meta_value',
*/
$visits_query = new WP_Query($visits_args); ?>

<main id="primary" class="site-main les-sejours">
    <header class="page-header">
        <h1 class="page-title"> Les Séjours</h1>
        <?php get_search_form() ?>
    </header><!-- .page-header -->
    <?php
    // The Loop
    if ($visits_query->have_posts()) {
        while ($visits_query->have_posts()) {
            $visits_query->the_post();
            $current_date = strtotime(get_field('start_date'));
            $current_month = date_i18n("F", $current_date);
            if ($visits_query->current_post === 0) {
                // If it's the first sejour we open the sejours-wrapper div and add a start date h5 separator
                echo '<h5 class="start-date">' . date_i18n("F Y", $current_date) . '</h5><div class="sejours-wrapper">';
            } else {
                $start_date = strtotime(get_field('start_date'));
                $f = $visits_query->current_post - 1;
                $last_post_ID = $visits_query->posts[$f]->ID;
                $old_date = strtotime(get_field('start_date', $last_post_ID));
                $old_month = date_i18n("F", $old_date);
                if ($current_month != $old_month) {
                    $current_month = $old_month;
                    // If it's the start date of the current sejour is different of the precedent we close the sejours-wrapper div and add a new one w/ a start date h5 separator
                    echo '</div><h5 class="start-date">' . date_i18n("F Y", $current_date) . '</h5><div class="sejours-wrapper">';
                    
                }
            }
            get_template_part('template-parts/content', 'sejours');
            
        }
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();
    ?>
</main><!-- #main -->
<?php
get_footer();
